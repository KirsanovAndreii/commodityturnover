package com.ankir33gmail.commodityturnover;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

/**
 * Created by AnKir on 12.05.2017.
 */

public class ListAdapter extends ArrayAdapter<Commodity> {
    public ListAdapter(@NonNull Context context, @LayoutRes @NonNull Commodity[] values) {
        super(context, R.layout.list_item, values);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        ViewHolder holder;
        View rowView = convertView;

        if (rowView == null) {
            rowView = LayoutInflater.from(getContext()).inflate(R.layout.list_item, parent, false);
            holder = new ViewHolder();

            holder.nameCommodity = (TextView) rowView.findViewById(R.id.nameCommodity);
            holder.amountCommodity = (TextView) rowView.findViewById(R.id.amountCommodity);
            rowView.setTag(holder);
        } else { holder = (ViewHolder) rowView.getTag();
        }

        holder.nameCommodity.setText(getItem(position).getName());
        holder.amountCommodity.setText(getItem(position).getAmount()+"");
        return rowView;
    }

    class ViewHolder {
        public TextView nameCommodity;
        public TextView amountCommodity;
    }
}
