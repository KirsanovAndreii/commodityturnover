package com.ankir33gmail.commodityturnover;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Random;

public class MainActivity extends AppCompatActivity {

    ListView listView;
    ListAdapter myListAdapter;
    int typeCommodity = 5;
    boolean onWork = true;
    TextView textViewWork;
    Commodity[] listCommodity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        textViewWork = (TextView) findViewById(R.id.text_view);
        listCommodity = createList(typeCommodity);

        listView = (ListView) findViewById(R.id.list_view);
        myListAdapter = new ListAdapter(this, listCommodity);
        listView.setAdapter(myListAdapter);

        BuyCommodity buyCommodity = new BuyCommodity();
        buyCommodity.execute();

    }


    // создание списка товаров
    private Commodity[] createList(int amoutCommodity) {
        Commodity[] rezult = new Commodity[amoutCommodity];
        for (int i = 0; i < amoutCommodity; i++) {
            Commodity nCommodity = new Commodity();
            nCommodity.setName("Commodity " + i);
            nCommodity.setAmount(500);
            rezult[i] = nCommodity;
        }
        return rezult;
    }

    public void onClickWorkBreak(View view) {
        onWork = !onWork;
        if (onWork) textViewWork.setText("Work");
        else textViewWork.setText("Break");
    }

    class BuyCommodity extends AsyncTask<Void, Integer, Void> {
        Random random = new Random();

        ArrayList<Integer[]> boxBrake = new ArrayList<>();

        @Override
        protected Void doInBackground(Void... params) {

            while (true) {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                int nCommodity = random.nextInt(typeCommodity - 1) + 1; // сколько товара продано за 1 раз(больше 0)
                Integer[] inquiry = new Integer[typeCommodity];// массив каких товаров продано
                for (int i = 0; i < inquiry.length; i++) inquiry[i] = 0; // обнуляем массив
                for (int i = 0; i < nCommodity; i++) {                   // заполняем массив проданных товаров
                    int numberCommodity = random.nextInt(typeCommodity);
                    inquiry[numberCommodity]++;
                }
                publishProgress(inquiry); // передаем массив закупку одного покупателя
            }
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);

            boxBrake.add(values);

            if (onWork)  // списываем товар, иначе накапливается очередь в boxBrake
            {
                for (int i = 0; i < boxBrake.size(); i++) {   //перебор покупателей
                    for (int j = 0; j < values.length; j++) {//перебор товара

                        listCommodity[j].setAmount(listCommodity[j].getAmount() - boxBrake.get(i)[j]);
                    }
                }
                myListAdapter.notifyDataSetChanged();
                boxBrake.clear(); //обнуление очереди
            }


        }

    }

}
